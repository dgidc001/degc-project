[1] Bennett, W.L., Wells, C. and Rank, A., 2009. Young citizens and civic learning: Two paradigms of citizenship  in the digital age. *Citizenship studies*, *13*(2), pp.105-120.

[2] Bogost, I., 2006. Playing politics: Videogames for politics, activism, and advocacy. *First Monday*.

[3] Christensen, H.S., 2011. Political activities on the Internet: Slacktivism or political participation by other means?. *First Monday*.

[4] Cohen, C.J., Kahne, J., Bowyer, B., Middaugh, E. and Rogowski, J., 2012. Participatory politics. *New media and youth political action*.

[5] Egenfeldt-Nielsen, S., Smith, J.H. & Tosca, S.P., 2013. *Understanding video games the essential introduction* Second., New York ; Oxfordshire, England: Routledge.

[6] Glenn, C.L., 2015. Activism or “Slacktivism?”: digital media and organizing for social change. *Communication Teacher*, *29*(2), pp.81-85.

[7] Juul, J., 2019. *Handmade pixels: Independent video games and the quest for authenticity*. Mit Press.

[8] Kahne, J., Middaugh, E. and Allen, D., 2015. Youth, new media, and the rise of participatory politics. *From voice to influence: Understanding citizenship in a digital age*, *35*.

[9] Kahne, J., Hodgin, E. and  Eidman-Aadahl, E., 2016. Redesigning civic education for the digital  age: Participatory politics and the pursuit of democratic engagement. *Theory & Research in Social Education*, *44*(1), pp.1-35.

[10] Kahne, J., Middaugh, E. and Evans, C., 2009. *The civic potential of video games* (p. 111). The MIT Press.

[11] Kligler-Vilenchik, N. (2017) 'Alternative citizenship models: Contextualizing new media and the new "good citizen"', [New Media and Society](http://journals.sagepub.com.ezproxy.is.ed.ac.uk/home/nms), Vol. 19, No. 11, pp. 1887–1903.